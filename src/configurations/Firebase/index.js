import firebase from 'firebase'

firebase.initializeApp({
    apiKey: "AIzaSyB0eApq5FV_hJKfTP6myclzaCBF9o8a9w0",
    authDomain: "idolist-4d987.firebaseapp.com",
    projectId: "idolist-4d987",
    storageBucket: "idolist-4d987.appspot.com",
    messagingSenderId: "77765491718",
    appId: "1:77765491718:web:0dd76237a9a8126fee9787",
    measurementId: "G-KF71HLFXRP"   
})

const Firebase = firebase;

export default Firebase;