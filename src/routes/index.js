import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Splash, Home, Calender} from '../pages';

const Stack = createStackNavigator();

const Routes = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false, title: 'Home'}}
      />
      <Stack.Screen
        name="Calender"
        component={Calender}
        options={{headerShown: false, title: 'Calender'}}
      />
    </Stack.Navigator>
    
  );
};

export default Routes;
