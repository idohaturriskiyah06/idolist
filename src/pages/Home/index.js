import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import calender from '../../assets/calender.png';
import logo from '../../assets/logo.png';
import add from '../../assets/add_icon.png';
import remove from '../../assets/delete_icon.png';
import blankTask from '../../assets/blank_task.png';
import CheckBox from 'react-native-check-box';

import Firebase from '../../configurations/Firebase';

class Home extends Component {
  state = {
    task: '',
    toDoTask: [],
  };

  componentDidMount() {
    let task = Firebase.database().ref('/toDoTask');
    task.once('value').then(snapshot => {
      this.setState({toDoTask: snapshot.val()});
    });
  }
  addTask = () => {
    if (this.state.task == '') {
      return false;
    }

    Firebase.database()
      .ref('/toDoTask')
      .push({
        activityList: this.state.task,
        completed: false,
      })
      .then(() => {
        let task = Firebase.database().ref('/toDoTask');
        task.once('value').then(snapshot => {
          this.setState({
            toDoTask: snapshot.val(),
          });
          console.log(snapshot.val());
        });

        this.setState({task: ''});
      })
      .catch(error => {
        alert(error);
      });
  };

  RadioCheck = list => {
    var completed = this.state.toDoTask[list].completed;
    var activityList = this.state.toDoTask[list].activityList;
    var finished = '';
    if (completed == true) {
      finished = false;
    } else {
      finished = true;
    }

    Firebase.database()
      .ref('/toDoTask' + '/' + list)
      .set({
        activityList: activityList,
        completed: finished,
      })
      .then(() => {
        let task = Firebase.database().ref('/toDoTask');
        task.once('value').then(snapshot => {
          this.setState({toDoTask: snapshot.val()});
        });
      })
      .catch(error => {
        alert(error);
      });
  };

  deleteTask = () => {
    let keyFirebase = Object.keys(this.state.toDoTask);
    let counter = 0;
    keyFirebase.map(list => {
      if (this.state.toDoTask[list].completed == true) {
        Firebase.database()
          .ref('/toDoTask' + '/' + list)
          .remove();
        counter++;
      }
    });

    let task = Firebase.database().ref('/toDoTask');
    task.once('value').then(snapshot => {
      this.setState({toDoTask: snapshot.val()});
    });
  };
  render() {
    let keyFirebase = [];
    if (this.state.toDoTask) {
      keyFirebase = Object.keys(this.state.toDoTask);
    }
    console.log(keyFirebase);
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image source={logo} style={{resizeMode: 'contain', width: 150}} />
        </View>
        <View style={styles.viewActivityList}>
          <ScrollView
            scrollEnabled
            showsVerticalScrollIndicator
            stickyHeaderIndices>
            {keyFirebase.length > 0 ? (
              keyFirebase.map(list => (
                <View key={list}>
                  <View style={styles.viewList}>
                    <CheckBox
                      onClick={() => this.RadioCheck(list)}
                      style={styles.checklist}
                      checkBoxColor="white"
                      isChecked={this.state.toDoTask[list].completed}
                    />
                    <Text style={styles.textList}>
                      {this.state.toDoTask[list].activityList}
                    </Text>
                    <TouchableOpacity
                      style={styles.Button}
                      onPress={this.deleteTask}>
                      <Image source={remove} style={styles.iconPack} />
                    </TouchableOpacity>
                  </View>
                </View>
              ))
            ) : (
              <View style={styles.emptyTask}>
                <Image
                  source={blankTask}
                  style={{resizeMode: 'contain', height: 200}}
                />
                <Text
                  style={{fontSize: 30, fontWeight: 'bold', color: '#fcc305'}}>
                  TUGAS KOSONG!
                </Text>
              </View>
            )}
          </ScrollView>
        </View>
        <View style={styles.footer}>
          <TouchableOpacity
            style={styles.Button}
            onPress={() => this.props.navigation.navigate('Calender')}>
            <Image source={calender} style={styles.iconPack} />
          </TouchableOpacity>
          <TextInput
            style={styles.textInput}
            placeholder="Tulis rencana...."
            placeholderTextColor="#8f6e00"
            onChangeText={text => this.setState({task: text})}
            value={this.state.task}
          />
          <TouchableOpacity style={styles.Button} onPress={this.addTask}>
            <Image source={add} style={styles.iconPack} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#b8e1e5',
  },
  viewActivityList: {
    flex: 1,
    borderTopRightRadius: 30,
    borderRadius: 20,
    backgroundColor: '#f7f1cb',
    marginHorizontal: 20,
    marginTop: 50,
    paddingTop: 50,
    paddingBottom: 20,
    padding: 20,
    shadowColor: 'rgb(0, 149, 179)',
    elevation: 3,
  },
  header: {
    flex: 0,
    height: 70,
    marginTop: 20,
    position: 'absolute',
    width: '60%',
    borderRadius: 20,
    backgroundColor: '#fcc305',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: 'grey',
    elevation: 5,
    zIndex: 1,
  },
  footer: {
    flex: 0,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    paddingVertical: 20,
  },
  Button: {
    backgroundColor: '#422f69',
    textAlign: 'center',
    height: 50,
    width: 50,
    borderRadius: 60,
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 200,
    shadowColor: 'rgb(0, 87, 105)',
    elevation: 3,
  },
  textInput: {
    backgroundColor: '#422f69',
    borderRadius: 60,
    paddingHorizontal: 20,
    paddingVertical: 15,
    fontSize: 15,
    fontWeight: 'bold',
    width: '68%',
    height: 50,
    shadowColor: 'rgb(0, 87, 105)',
    elevation: 3,
    color: '#fcc305',
  },
  viewList: {
    flexDirection: 'row',
    padding: 3,
    marginVertical: 5,
    marginHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#fcc305',
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  textList: {
    flex: 4,
    paddingVertical: 5,
    paddingHorizontal: 5,
    fontSize: 18,
    fontWeight: 'bold',
    color: '#422f69',
    paddingStart: 15,
  },
  checklist: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    backgroundColor: '#422f69',
    borderRadius: 60,
  },

  iconPack: {
    resizeMode: 'center',
    height: 25,
    width: 25,
  },
  emptyTask: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 150,
  },
});
