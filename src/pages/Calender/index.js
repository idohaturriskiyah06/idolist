import { Calendar, CalendarList, LocaleConfig} from 'react-native-calendars';
import { View } from 'react-native';
import React from 'react';

const Calender = () => {
    return (
      <View style={{ paddingTop: 50, flex: 1 }}>
        <CalendarList
        onVisibleMonthsChange = {(months) => {console.log('now these months are visible', months);}}
        pastScrollRange = {0}
        futureScrollRange = {10}
        scrollEnabled = {true}
        showScrollIndicator = {false}
        maxDate = {new Date()}
        />

      </View>
    );
}

export default Calender;
